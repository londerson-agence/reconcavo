#ifndef REQUEST_H
#define REQUEST_H

#define BUFFER_SIZE 1024

class Request {
  private:
	char buffer[BUFFER_SIZE];
  int socket_conn;

  public:
  Request(int sock_conn);
  void get();
  void print();
};

#endif
