#ifndef SOCKET_H
#define SOCKET_H

class Socket {
  private:
	struct hostent *hp;
	struct sockaddr_in addr;
	int on = 1, port = 80, sock;

  public:
  Socket(char *host);
  int conn();

  private:
  void set_conn();
  void set_hostent(char *host);
  void set_sock();
};

#endif
