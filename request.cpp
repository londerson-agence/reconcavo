#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>

#include "request.h"

#define BUFFER_SIZE 1024

Request::Request(int sock_conn) {
  socket_conn = sock_conn;
}

void Request::get() {
  write(socket_conn, "GET /\r\n", strlen("GET /\r\n")); // write(fd, char[]*, len);
  bzero(buffer, BUFFER_SIZE);
}

void Request::print() {
  while(read(socket_conn, buffer, BUFFER_SIZE - 1) != 0){
    fprintf(stderr, "%s", buffer);
    bzero(buffer, BUFFER_SIZE);
  }
}
