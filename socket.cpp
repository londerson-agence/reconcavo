#include <string.h>
#include <netinet/tcp.h>
#include <netdb.h>

#include "socket.h"

Socket::Socket(char *host) {
  set_hostent(host);
  set_sock();
}

int Socket::conn() {
  set_conn();
  connect(sock, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
  return sock;
}

void Socket::set_conn() {
  bcopy(hp->h_addr, &addr.sin_addr, hp->h_length);
  addr.sin_port = htons(port);
  addr.sin_family = AF_INET;
  setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (const char *)&on, sizeof(int));
}

void Socket::set_hostent(char *host) {
  hp = gethostbyname(host);
}

void Socket::set_sock() {
  sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
}
