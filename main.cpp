#include <unistd.h>
#include <netdb.h>

#include "socket.h"
#include "request.h"

int main(int argc, char *argv[]){
	int socket_conn;
  char *host = argv[1];

  Socket socket(host);
  socket_conn = socket.conn();

  Request req(socket_conn);
  req.get();
  req.print();

	shutdown(socket_conn, SHUT_RDWR);
	close(socket_conn);
}

//make url=www.cplusplus.com
//g++ -std=c++11 main.cpp -o main && ./main www.cplusplus.com
